#include <iostream>
#include <string>
#include <omp.h>

using namespace std;

void fillVec(int *vec, int n, int max) {
  for (int i = 0; i < n; i++) {
    vec[i] = rand() % max;
  }
}

void print(string pref, int *vec, int n) {
  cout << pref;
  for (int i = 0; i < n; i++)
    cout << vec[i] << " ";
  cout << endl;
}

void add(int *a, int *b, int *c, int n) {
  for (int i = 0; i < n; i++)
    c[i] = a[i] + b[i];
}

void addOmp(int *a, int *b, int *c, int n) {
  #pragma omp parallel for
  for (int i = 0; i < n; i++)
    c[i] = a[i] + b[i];
}

int main(int argc, char const *argv[]) {
  if (argc < 2) 
    cout << "I need more inputs" << endl;
  else {
    int vec_size = stoi(argv[1]);
    string test = argv[2];
    int *a, *b, *c;
    double begin, end;

    a = new int[vec_size];
    b = new int[vec_size];
    c = new int[vec_size];

    fillVec(a, vec_size, 10);
    fillVec(b, vec_size, 10);

    //print("a = ", a, vec_size);
    //print("b = ", b, vec_size);

    if (test == "sequential") {
      begin = omp_get_wtime();
      add(a, b, c, vec_size);
      end = omp_get_wtime();
      cout << end- begin << endl;
    } else if (test == "parallel") {
      begin = omp_get_wtime();
      addOmp(a, b, c, vec_size);
      end = omp_get_wtime();
      cout << end - begin << endl;
    }
    //print("c = ", c, vec_size);

    delete(a);
    delete(b);
    delete(c);
  }

  return 0;
}