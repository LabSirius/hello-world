#include <iostream>
#include <string>
#include <omp.h>

using namespace std;

int main() {
  int iam, nt;

  #pragma omp parallel default(shared) private(iam,nt) 
  {
    iam = omp_get_thread_num();
    nt =  omp_get_num_threads();
    string out =  "I'm Thread -> " + to_string(iam) + "/" + to_string(nt) + "  ";
    cout << out << endl;
  }

  return 0;
}