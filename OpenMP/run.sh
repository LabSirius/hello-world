#!/bin/bash

if [ -e out.txt ]
then
  rm out.txt
fi

sizes=( 10 100 1000 10000 100000 1000000 )

for size in "${sizes[@]}"
do
  for loop in {1..10}
  do
    ./a.out $loop sequential >> out.txt
  done
done

for size in "${sizes[@]}"
do
  for loop in {1..10}
  do
    ./a.out $loop parallel >> out.txt
  done
done