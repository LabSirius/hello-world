# OpenMP

## Código

Incluir módulo OpenMP

```cpp
#include <omp.h>
.
.
.
```

## Compilar

```bash
g++ <filename>.cpp -o <exe>.out -fopenmp
```

## Ejecutar

```bash
./<exe>.out
```